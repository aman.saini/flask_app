FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3 python3-pip build-essential libssl-dev libffi-dev python3-dev
WORKDIR /app
COPY . /app
RUN pip install -r "Requirements.txt"
ENTRYPOINT ["python3"]
CMD ["App.py"]
